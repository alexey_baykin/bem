function [ phi ] = Calc_Angle_Btw_Vectors( x1,y1,x2,y2 )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here


    dot_product = Dot_Product( x1,y1,x2,y2 );
    norm1 = sqrt( Dot_Product( x1,y1,x1,y1 ) );
    norm2 = sqrt( Dot_Product( x2,y2,x2,y2 ) );
    
    cos_phi = dot_product / (norm1*norm2);
    
    phi = acos(cos_phi);
    
    cross_product = Cross_Product( x1,y1,x2,y2 );

    if ( cross_product < 0.0)
        phi = 2*pi-phi;
    end

end

