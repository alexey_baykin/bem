function [ Gii ] = Calc_Gii_Laplace( x1,y1,x2,y2 )
%UNTITLED15 Summary of this function goes here
%   Detailed explanation goes here
    
    Lj = sqrt((x2-x1)^2 + (y2-y1)^2);
    
    %Lj
   
    Gii = Lj/(2*pi)*(log(Lj/2)-1.0);
end

