function [ Gij ] = Calc_Gij_Laplace( x,y,x1,y1,x2,y2 )
%UNTITLED15 Summary of this function goes here
%   Detailed explanation goes here

    Lj2 = (x2-x1)^2 + (y2-y1)^2;
    a = 0.25*Lj2;
    b = 1/2 * (-x1^2 + 2*x*(x1 - x2) + x2^2 + 2*y*y1 - y1^2 - 2*y*y2 + y2^2);
    c = 0.25 * ( (-2*x + x1 + x2)^2 + (-2*y + y1 + y2)^2 );
    Dsqrt = sqrt(4*a*c-b^2); % sqrt of -discriminant 
    
    % I1(1)
    I1_1 =  -2 + ...
            Dsqrt / a * atan((b+2*a)/Dsqrt) +...
            (b+2*a)/(2*a)*log((a+b+c));
    % I1(-1)
    I1_min1 =  2 + ...
            Dsqrt / a * atan((b-2*a)/Dsqrt) +...
            (b-2*a)/(2*a)*log((a-b+c));

    Gij = 0.5*sqrt(Lj2)/(4*pi)*(I1_1-I1_min1);

end

