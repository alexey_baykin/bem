function [ Gij ] = Calc_Gij_Laplace_Numerically( x,y,x1,y1,x2,y2 )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    import gauss_integration_package.*;
   
    AX= (x2-x1)/2.0;
    AY= (y2-y1)/2.0;
    BX= (x2+x1)/2.0;
    BY= (y2+y1)/2.0;

    gxi = gauss_integrator_1d.gxi;
    gw = gauss_integrator_1d.gw;
    RESULT = 0.0;

    for i =1:4
        
        XCI =AX*gxi(i) +BX;
        YCI =AY*gxi(i) +BY;
        RA=sqrt( (XCI-x)^2+ (YCI -y)^2);
        RESULT=RESULT+log(RA)*gw(i);
        
        
    end
    
    SL=2.0*sqrt(AX^2+AY^2);
    Gij=RESULT*SL/(4.0*pi);

end

