function [ Hij ] = Calc_Hijhat_Laplace( x,y,x1,y1,x2,y2 )  
% See formalas (4.24), (4.25) on p.55 in...
% J. Katsikadelis Boundary Elements: Theory and Applications, 1st  
% Edition, 2002
% WARNING! Applicable only if points (x,y), (x1,y1), (x2,y2) do not lie on one
% line
   
    v1x = x1-x;
    v1y = y1-y;
    
    v2x = x2-x;
    v2y = y2-y;
    
    phi = Calc_Angle_Btw_Vectors( v1x,v1y,v2x,v2y );
    phi1 = dAlpha( v1x,v1y,v2x,v2y );
    %ss = phi-phi1
     if(abs(phi-phi) > 1e+5)
         xx=1;
         
     end
    
    %phi1_grad = phi*180/pi;
    %phi = atan((y2-y)/(x2-x)) - atan((y1-y)/(x1-x));
    
    
%     a2 = Calc_Angle_Btw_Vectors( 1,0,v2x,v2y );
%     a1 = Calc_Angle_Btw_Vectors( 1,0,v1x,v1y );
%     
%     a2gr = a2*180/pi;
%     a1gr = a1*180/pi;
%     %phi = a2-a1;
%     
%     a = [v1x,v1y,0.0];
%     b = [v2x,v2y,0.0];
%     angle = atan2(norm(cross(a,b)),dot(a,b));
    
%     if(abs(phi-phi) > 1e+5)
%         
%         
%         figure;
%         %plot([0,1],[0,0],'m');
%         %hold on;
%         
%         rr = max(sqrt(v1x^2+v1y^2),sqrt(v2x^2+v2y^2));
%         plot([0,v1x/rr],[0,v1y/rr],'b');
%         hold on;
%         plot([0,v2x/rr],[0,v2y/rr],'r');
%         hold off;
%         xlim([-1,1]);
%         ylim([-1,1]);
%         legend('1','2');
%         phi = Calc_Angle_Btw_Vectors( v1x,v1y,v2x,v2y );
%     end
    Hij = phi / (2.0*pi);
end

