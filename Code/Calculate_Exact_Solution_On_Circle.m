function [ u,dudn ] = Calculate_Exact_Solution_On_Circle( xp,yp,R )
%UNTITLED18 Summary of this function goes here
%   Detailed explanation goes here

    N = length(xp)-1;
    u = zeros(N,1);
    dudn = zeros(N,1);
    for i=1:N
        x = (xp(i) + xp(i+1)) / 2.0;
        y = (yp(i) + yp(i+1)) / 2.0;
        
        r = sqrt(x^2 + y^2);
        cos_phi = x/r;
        
        u(i) = r/R*cos_phi;
        dudn(i) = 1/R*cos_phi;
        %0.503295692732039
        %0.5132
       %0.5233
    end

end

