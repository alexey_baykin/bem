
is_show_plots = ~false;

G = 3;
nu=0.18;
L = 1;
N = 3200;

p = -1;
x_min = -L;
x_max = L;
h = (x_max-x_min) / N;

A = zeros(N,N);
rhs = zeros(N,1);
rhs(1:end) = p;
a = h/2;
for i=1:N
    xi = -L+ h/2 + (i-1)*h;
    for j=1:N
        
        xj = -L+ h/2 + (j-1)*h;
        
        A(i,j) = G / pi/(1.0-nu)*a/((xi-xj)^2-a^2);
    end
end

u = A \ rhs;
x = -L+ h/2 + (0:(N-1))*h;

Nout = N;
hout = (x_max-x_min) / Nout;
x_out = -L+ hout/2 + (0:(Nout-1))'*hout;
u_exact = -2*(1-nu)/G*p*sqrt(L^2-x_out.^2);

max(abs(u-u_exact))

x_out2 = -L+ (0:(Nout))*hout;
u_exact2 = -2*(1-nu)/G*p*sqrt(L^2-x_out2.^2);

if( is_show_plots )

    figure;
    plot(x,u,'r');
    hold on;
    plot(x_out2,u_exact2);
end