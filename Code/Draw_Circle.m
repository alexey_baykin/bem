function [  ] = Draw_Circle( axes_h,R )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    Nphi = 1000;
    dang = 2*pi / Nphi;
    x0 = 0.0;
    y0 = 0.0;

    ang=0:dang:2*pi; 

    xp=R*cos(ang);
    yp=R*sin(ang);

    plot(axes_h,x0+xp,y0+yp,'r');
    
end

