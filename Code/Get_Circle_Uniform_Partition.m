function [ xp,yp ] = Get_Circle_Uniform_Partition( R,N )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    xp = zeros(1,N); 
    yp = zeros(1,N);
    
    dphi = 2*pi/N;
    phi = 0.0:dphi:(2.0*pi);
    
    xp = R*cos(phi);
    yp = R*sin(phi);

end

