%% In this script the Laplace equation is solved using BEM

is_show_plots = ~false;

N = 50; % [], Number of elements
R = 5; % [m], Circle radius

[ xp,yp ] = Get_Circle_Uniform_Partition( R,N );
[ u_exact,dudn_exact ] = Calculate_Exact_Solution_On_Circle( xp,yp,R );
%figure;
%Draw_Closed_Polyline( gca,xp,yp );
%hold on;
%Draw_Circle( gca,R );

N1 = 0;
N2 = N-N1;

u_known_index = 1:N1;
u_n_known_index = (N1+1):N;
u_known = u_exact(u_known_index);
dudn_known = dudn_exact(u_n_known_index);

[u,dudn]  = Run_BEM_Laplace( u_known,u_known_index,dudn_known,u_n_known_index,xp,yp );

max(abs(dudn-dudn_exact))

if( is_show_plots )
    figure;
    plot(dudn,'r');
    hold on;
    plot(dudn_exact);

    figure;
    plot(u,'r');
    hold on;
    plot(u_exact);
end



