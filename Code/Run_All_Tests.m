addpath('./TESTS');

tests_list = {
    'TEST_Hijhat_Laplace',...
    'TEST_Gij_Gii_Laplace',...
    'TEST_Convergence_Laplace_Dirichlet',...
    'TEST_Convergence_Laplace_Mixed'...
    };

for i=1:length(tests_list)
    
    test_run_str=['is_success = ' tests_list{i} '();'];
    eval(test_run_str); % run the current test
    
    if(is_success==true)
      exit_status_str =   'SUCCESS';
    else
      exit_status_str =   'FAILED';
    end
    
    test_status_str = ['Test ' tests_list{i} ' : ' exit_status_str ];
    disp(test_status_str);
    
end