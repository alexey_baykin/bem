function [ u,dudn ] = Run_BEM_Laplace( u_known,u_known_index,dudn_known,dudn_known_index,xp,yp )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    N = length(xp)-1;
    [ H,G ] = Set_Laplace_Dirichlet_Problem( xp,yp );
    
    A = zeros(N,N);
    B = zeros(N,N);
    known_quantity = zeros(N,1);
    
    N1 = length(u_known_index);
    N2 = length(dudn_known_index);
    
    
    known_quantity(u_known_index) = u_known(1:N1);
    known_quantity(dudn_known_index) = dudn_known(1:N2);
    
    A(1:end,dudn_known_index) = H(1:end,dudn_known_index);
    A(1:end,u_known_index) = -G(1:end,u_known_index);
    
    B(1:end,u_known_index) = -H(1:end,u_known_index);
    B(1:end,dudn_known_index) = G(1:end,dudn_known_index);
    
    
    
    unknown_quantity = A\(B*known_quantity); 
    
    u = zeros(N,1);
    dudn = zeros(N,1);
    u(u_known_index) = known_quantity(u_known_index);
    u(dudn_known_index) = unknown_quantity(dudn_known_index);
   
    dudn(dudn_known_index) = known_quantity(dudn_known_index);
    dudn(u_known_index) = unknown_quantity(u_known_index);
    
    

end

