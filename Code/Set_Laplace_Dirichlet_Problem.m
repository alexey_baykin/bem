function [ H,G ] = Set_Laplace_Dirichlet_Problem( xp,yp )
% This funtion sets H and G matrices for Laplace equation.
% The boundary is an arbitrary simple connected domain.
% INPUT:
%        xp,yp - 1d arrays with size N+1, N - number of boundary segments. 
%        Contain coordinates of boundary points (x_i,y_i).
%        The boundary is closed: xp(N+1) = xp(1),yp(N+1) = yp(1).
% OUTPUT:   
%       H(N,N), G(N,N) - matrices in BEM:
%           H*u = G*du/dn on boundary
    N = length(xp)-1;

    H = zeros(N,N);
    G = zeros(N,N);
    for i=1:N
        x = (xp(i) + xp(i+1)) / 2.0;
        y = (yp(i) + yp(i+1)) / 2.0;
        for j=1:N
            if(i~=j)
                H(i,j) = Calc_Hijhat_Laplace( x,y,xp(j),yp(j),xp(j+1),yp(j+1) );
                G(i,j) = Calc_Gij_Laplace( x,y,xp(j),yp(j),xp(j+1),yp(j+1) );
            else
                H(i,j) = Calc_Hiihat_Laplace(  ) - 1.0/2.0;
                G(i,j) = Calc_Gii_Laplace( xp(j),yp(j),xp(j+1),yp(j+1) );
            end
            
        end       
    end

end

