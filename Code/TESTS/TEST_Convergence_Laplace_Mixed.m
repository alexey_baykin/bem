function [ is_success ] = TEST_Convergence_Laplace_Mixed()
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    is_success = true;

    rel_tol = 0.05;
    N0 = 50;
    K = 3;

    rel_du_dn_max_error = zeros(1,K);
    rel_u_max_error = zeros(1,K);

    for k=1:K
        N = N0*2^(k-1); % [], Number of elements
        R = 1; % [m], Circle radius

        [ xp,yp ] = Get_Circle_Uniform_Partition( R,N );
        [ u_exact,dudn_exact ] = Calculate_Exact_Solution_On_Circle( xp,yp,R );

        u_known_index = 1:2:N;
        u_n_known_index = 2:2:N;
        u_known = u_exact(u_known_index);
        dudn_known = dudn_exact(u_n_known_index);

        [u,dudn]  = Run_BEM_Laplace( u_known,u_known_index,dudn_known,u_n_known_index,xp,yp );

        rel_du_dn_max_error(k) = max(abs(dudn-dudn_exact)) / max(abs(dudn_exact));
        rel_u_max_error(k) = max(abs(u-u_exact)) / max(abs(u_exact));
    end

    convergence_order_dudn = zeros(1,K-1);
    convergence_order_u = zeros(1,K-1);
    for k=1:K-1
        convergence_order_dudn(k) = log(rel_du_dn_max_error(k)/rel_du_dn_max_error(k+1))/ log(2);
        convergence_order_u(k) = log(rel_u_max_error(k)/rel_u_max_error(k+1))/ log(2);
    end


    min_convergence_order_du_dn = min(convergence_order_dudn);
    min_convergence_order_u = min(convergence_order_u);

    accepted_convergence_order_du_dn = 2-rel_tol;
    accepted_convergence_order_u = 3-rel_tol;

    if( (min_convergence_order_du_dn < accepted_convergence_order_du_dn) ||...
        (min_convergence_order_u < accepted_convergence_order_u)   ) 
        is_success = false;
    end
    
end

