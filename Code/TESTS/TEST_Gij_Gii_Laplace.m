function [ is_success ] = TEST_Gij_Gii_Laplace()
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
    
    is_success = true;
    
    rel_tol = 1.0e-14;
    
    x = 3;
    y = 4;
    
    x1 = 5;
    y1 = 2;
    
    x2=1;
    y2=7;

    Gij = Calc_Gij_Laplace( x,y,x1,y1,x2,y2 );
    Gij_exact = 0.3256184236828768;
    rel_error_Gij = abs(Gij-Gij_exact)/abs(Gij_exact);
    
    Gii =  Calc_Gii_Laplace( x1,y1,x2,y2 );
    Gii_exact = 0.1667625341723108;
    rel_error_Gii = abs(Gii-Gii_exact)/abs(Gii_exact);
    
    rel_error_max = max(rel_error_Gij,rel_error_Gii);
    
    if( rel_error_max > rel_tol )
        
        is_success = false;

    end

end

