function [ is_success ] = TEST_Hijhat_Laplace()
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
    
    is_success = true;
    
    rel_tol = 1.0e-14;
    
    x = 0.0;
    y = 0.0;
    R = 1.0;
    N = 10;
    
    dphi = 2*pi/N;
    phi = 0.0:dphi:(2.0*pi);
    
    xp = R*cos(phi);
    yp = R*sin(phi);
    
    for i=1:N
    
        Hij = Calc_Hijhat_Laplace( x,y,xp(1),yp(1),xp(i+1),yp(i+1) );
        Hij_exact = dphi/(2*pi)*i;
        
        if( abs(Hij-Hij_exact)/abs(Hij_exact) > rel_tol )
            is_success = false;
            break;
            
        end
         
    end
    
end

